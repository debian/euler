/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : scalp.h -- long accumulator
 */

#ifndef _SCALP_H_
#define _SCALP_H_

#include "stack.h"

void accuinit ();

void maccuload (header *hd);
void maccuadd (header *hd);
void maccuload2 (header *hd);
void maccuadd2 (header *hd);
void mresiduum (header *hd);
void maccu1 (header *hd);
void maccu2 (header *hd);


#endif
