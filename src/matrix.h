/*
 *	Euler - a numerical lab
 *
 *	file : matrix.h -- builtin matrix functions
 */
 
#ifndef _MATRIX_H_
#define _MATRIX_H_

#include "stack.h"

void minmax (double *x, long n, double *min, double *max,
	int *imin, int *imax);

void msize (header *hd);
void mcols (header *hd);
void mrows (header *hd);
void mredim (header *hd);
void mresize (header *hd);

void vectorize (header *init, header *step, header *end);
void mdup (header *hd);
void mmatrix (header *hd);
void mzerosmat (header *hd);
void mones (header *hd);
void mdiag (header *hd);
void msetdiag (header *hd);
void mdiag2 (header *hd);
void mband (header *hd);

void mscompare (header *hd);
void mfind (header *hd);
void mextrema (header *hd);
void mnonzeros (header *hd);
void many (header *hd);

void transpose (header *hd);
void msum (header *hd);
void mprod (header *hd);
void mcumsum (header *hd);
void mcumprod (header *hd);
void mflipx (header *hd);
void mflipy (header *hd);
void mrotleft (header *hd);
void mrotright (header *hd);
void mshiftleft (header *hd);
void mshiftright (header *hd);
void mvconcat (header *hd);
void mhconcat (header *hd);

void wmultiply (header *hd);
void smultiply (header *hd);

#endif
