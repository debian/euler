/*
 *	metagtk.h
 *
 *	gtk graphic device : window for graphic output
 */

#ifndef __GTK_META_H__
#define __GTK_META_H__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include "meta.h"

#define GTK_META(obj)          GTK_CHECK_CAST (obj, gtk_meta_get_type (), GtkMeta)
#define GTK_META_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_meta_get_type (), GtkMetaClass)
#define GTK_IS_META(obj)       GTK_CHECK_TYPE (obj, gtk_meta_get_type ())

typedef struct _GtkMeta        GtkMeta;
typedef struct _GtkMetaClass   GtkMetaClass;

struct _GtkMeta
{
	GtkWidget	widget;
	
	gint		width, height;
	
	metadevice	dev;
	
	GdkFont		*font;
	GdkColormap *cmap;
	GdkColor	*colors;
	GdkGC 		*gc, *stipplegc;
	
	GdkCursor *	cursor;

	GdkDrawable *draw;
	GdkPixmap	*pixmap;
	int			playing;
	
	double		x, y;		/* mouse x and y */
};

struct _GtkMetaClass
{
	GtkWidgetClass parent_class;
};


GType			gtk_meta_get_type			(void);

GtkWidget*		gtk_meta_new				(gint width, gint height);

metadevice *	gtk_meta_get_device			(GtkWidget *widget);
void			gtk_meta_update_lines		(GtkWidget *widget);
void			gtk_meta_update_colors		(GtkWidget *widget);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif
