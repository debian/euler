/*
 *	Euler - a numerical lab
 *
 *	platform : all
 *
 *	file : command.h -- builtin command handling
 */

#ifndef _COMMAND_H_
#define _COMMAND_H_

#include "stack.h"

extern char* path[32];
extern int npath;

typedef enum {
	c_none,
	c_allv,
	c_quit,
	c_hold,
	c_shg,
	c_load,
	c_udf,
	c_return,
	c_for,
	c_end,
	c_break,
	c_loop,
	c_if,
	c_repeat,
	c_endif,
	c_else,
	c_elseif,
	c_clear,
	c_clg,
	c_cls,
	c_exec,
	c_forget,
	c_global
} comtyp;

typedef struct {
	char *name;
	comtyp nr;
	void (*f)(void);
} commandtyp;

extern commandtyp command_list[];

void sort_command (void);
commandtyp *preview_command (unsigned long *l);

#endif
