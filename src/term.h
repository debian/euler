/*************************************************************************
 *
 *	GTK Euler : the notebook widget
 *
 *************************************************************************/

#ifndef __GTK_TERM_H__
#define __GTK_TERM_H__

#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include "earray.h"


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GTK_TERM(obj)          GTK_CHECK_CAST (obj, gtk_term_get_type (), GtkTerm)
#define GTK_TERM_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, gtk_term_get_type (), GtkTermClass)
#define GTK_IS_TERM(obj)       GTK_CHECK_TYPE (obj, gtk_term_get_type ())


typedef struct _GtkTerm        GtkTerm;
typedef struct _GtkTermClass   GtkTermClass;


struct _GtkTerm
{
	GtkWidget	widget;
	
	GtkWidget    *p_scroll;     /* pointer to scrollbar */
	
	GtkAdjustment *v_adj;		/* scrollbar adjustment */
	
	GdkWindow *	text;
	
	GString *	name;			/* current notebook filename */
	
	earray *	a;				/* the variable which contains the notebook data */

	gint		top;			/* the index of top visible line */
	gint		cur, pos, epos;	/* the current line, pos and end pos (!= pos if a selection is active) */
	gint		promptlen;		/* length of prompt string */
	gint		xoff;			/* x offset */
	gint		xcaret, ycaret;	/* caret position */
	
	gint		twidth;			/* width in char */
	gint		theight;		/* height in char */
	
	GString *	tfont;
	GdkFont	*	font;
	guint		cwidth, cheight;
	
	GdkGC 		*commentGC, *outputGC, * promptGC, *udfGC, *highlightGC;
	
	GdkCursor *	cursor;
	
	gint		scan, code;		// scan key code, and ascii code of the last key pressed
	
	gint		editing, initializing, selecting;
	
	gint		timeout_id;		/* blinking time out */
	guint		caret_blink_state:1;	/* caret blink state */
	
	earray *	history;		/* command history handling */
	int			max_history;
	int			hist;
	
	GString *	clipboard;
	
	int			changed;
	GtkMenu *   menu;
};

struct _GtkTermClass
{
	GtkWidgetClass parent_class;
	
	void (*changed)(GtkTerm *term);
	void (*saved)(GtkTerm *term);
	void (*editing)(GtkTerm *term);
	void (*interpreting)(GtkTerm *term);
};


GType			gtk_term_get_type			(void);

GtkWidget*		gtk_term_new				(guint cols, guint rows, char *font);

gint			gtk_term_load				(GtkWidget *widget, gchar *filename);
gint			gtk_term_save				(GtkWidget *widget, gchar *filename);
void			gtk_term_clear				(GtkWidget *widget);
void			gtk_term_clear_new			(GtkWidget *widget);

gchar *			gtk_term_get_comment		(GtkWidget *widget);
void			gtk_term_set_comment		(GtkWidget *widget, gchar *cmt);

gchar *			gtk_term_get_name			(GtkWidget *widget);
gint			gtk_term_is_named			(GtkWidget *widget);
gint			gtk_term_is_changed			(GtkWidget *widget);
gint			gtk_term_is_initialized		(GtkWidget *widget);
gint			gtk_term_is_editing			(GtkWidget *widget);

void			gtk_term_print				(GtkWidget *widget, gchar *text);
void			gtk_term_edit_on			(GtkWidget *widget);
void			gtk_term_edit_off			(GtkWidget *widget);

void			gtk_term_set_colors			(GtkWidget *widget,
											 GdkColor *cmdColor,
											 GdkColor *outColor,
											 GdkColor *cmtColor,
											 GdkColor *udfColor);

void			gtk_term_copy				(GtkWidget *widget);
void			gtk_term_cut				(GtkWidget *widget);
void			gtk_term_paste				(GtkWidget *widget);

void 			gtk_term_insert_command		(GtkWidget *widget, char *text);
void			gtk_term_delete_command		(GtkWidget *widget);

void			gtk_term_delete_current_output		(GtkWidget *widget);
void			gtk_term_delete_outputs		(GtkWidget *widget);

void			gtk_term_set_popup			(GtkWidget *widget, GtkMenu *menu);

void			gtk_term_set_scrollbar		(GtkWidget *widget, GtkWidget *scroll);

gint            gtk_term_redraw             (GtkWidget *widget);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GTK_TERM_H__ */
