/*
 *	Euler - a numerical lab
 *
 *	file : sysdep.h -- system dependant functions
 */

#ifndef _SYSDEP_H_
#define _SYSDEP_H_

#include <math.h>
#include <ctype.h>

#ifndef M_PI
#define M_PI            3.14159265358979323846
#endif

#define TAB 9

#ifndef ALIGNMENT
#define ALIGNMENT 8
#endif

#if defined __MSDOS__
#define SPLIT_MEM
#define FLOAT_TEST
#endif

#ifdef unix
#define PATH_DELIM_CHAR '/'
#define PATH_DELIM_STR "/"
#else
#define PATH_DELIM_CHAR '\\'
#define PATH_DELIM_STR "\\"
#endif

extern int nojump;
extern int linelength;

typedef enum {
	line_none,
	line_solid,
	line_dotted,
	line_dashed,
	line_arrow
} linetyp;

typedef enum {
	marker_cross,
	marker_circle,
	marker_diamond,
	marker_dot,
	marker_plus,
	marker_square,
	marker_star
} markertyp;

typedef enum {
	bar_solid,
	bar_framed,
	bar_frame,
	bar_vhatch,
	bar_hhatch,
	bar_diagonal1,
	bar_diagonal2,
	bar_cross
} bartyp;

typedef enum {
	fill_blank,
	fill_filled
} filltyp;

typedef enum {
	key_none,
	cursor_up,
	cursor_down,
	cursor_left,
	cursor_right,
	escape,
	deletekey,
	backspace,
	clear_home,
	switch_screen,
	enter,
	space,
	line_end,
	line_start,
	fk1,
	fk2,
	fk3,
	fk4,
	fk5,
	fk6,
	fk7,
	fk8,
	fk9,
	fk10,
	word_left,
	word_right,
	help
} scantyp;

void mouse (double *, double *);

void getpixelsize (double *x, double *y);

void text_mode (void);
void graphic_mode (void);

int wait_key (int *scan);
int test_key (void);
int test_code (void);

char *cd (char *dir);
int scan_dir(char *dir_name, char *pat, char ** files[], int *files_count);

void clear_screen (void);
int execute (char *, char *);
int shrink (size_t size);

double myclock (void);

void gprint (char *s); /* print an output text (no newline) */
void gflush (void); /* flush out graphics */

void move_cr (void);
void move_cl (void);
void clear_eol (void);
void cursor_off (void); 
void cursor_on (void);

void edit_on (void);
void edit_off (void);

void sys_wait (double delay, int *scan);

void set_editline (char *p);

#ifdef WAVES
void sys_playwav (char *file);
#endif

double getcolor (int i, int j);

#endif
