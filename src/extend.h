#ifndef _EXTEND_H_
#define _EXTEND_H_

#include "stack.h"

#ifdef WAVES
void mplaywav (header *hd);
#endif
#ifdef DLL
void mdll (header *hd);
#endif

#endif
