/*************************************************************************
 *	metaps
 *
 *	metafile postscript output driver
 *
 *	Eric Bouchar�, 08/08/2001
 *
 *************************************************************************/

#ifndef PSGRAPH_H
#define PSGRAPH_H

#include <stdio.h>
#include "sysdep.h"

int dump_postscript(char *filename);


#endif

