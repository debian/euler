/*
 *	Euler - a numerical lab
 *
 *	file : funcs.h -- builtin functions
 */

#ifndef _FUNCS_H_
#define _FUNCS_H_

#include "stack.h"

/*
 *	basic ops
 */
void complex_multiply (double *x, double *xi, double *y, double *yi,
	double *z, double *zi);
void complex_divide (double *x, double *xi, double *y, double *yi,
	double *z, double *zi);
void copy_complex (double *x, double *y);

void add (header *hd, header *hd1);
void subtract (header *hd, header *hd1);
void dotmultiply (header *hd, header *hd1);
void dotdivide (header *hd, header *hd1);


/*
 *	constants
 */
void mpi (header *hd);
void mtime (header *hd);
void mfree (header *hd);
void mepsilon (header *hd);
void msetepsilon (header *hd);
void mlocalepsilon (header *hd);


/*
 *	types
 */
void mtype(header *hd);
void misreal (header *hd);
void misinterval (header *hd);
void miscomplex (header *hd);
void misvar (header *hd);
void misfunction (header *hd);

/*
 *	math for real, complex, interval
 */

void msin (header *hd);
void mcos (header *hd);
void mtan (header *hd);
void masin (header *hd);
void macos (header *hd);
void matan (header *hd);
void mexp (header *hd);
void mlog (header *hd);
void msqrt (header *hd);
void mpower (header *hd);

void mmod (header *hd);
void mintersects (header *hd);

void mdegree (header *hd);

void msign (header *hd);
void mceil (header *hd);
void mfloor (header *hd);
void mround (header *hd);

void mcomplex (header *hd);
void mconj (header *hd);
void mre (header *hd);
void mim (header *hd);
void marg (header *hd);
void mabs (header *hd);

/*
 *	compare operators
 */
void mgreater (header *hd);
void mless (header *hd);
void mgreatereq (header *hd);
void mlesseq (header *hd);
void mequal (header *hd);
void munequal (header *hd);
void maboutequal (header *hd);

/*
 *	logical operators
 */
void mnot (header *hd);
void mand (header *hd);
void mor (header *hd);

/*
 *	statistics - random numbers
 */
void mfastrandom (header *hd);
void mnormal (header *hd);
void mfak (header *hd);
void mlogfak (header *hd);
void mbin (header *hd);
void mlogbin (header *hd);
void mtd (header *hd);
void minvgauss (header *hd);
void minvtd (header *hd);
void mchi (header *hd);
void mfdis (header *hd);
void mstatistics (header *hd);

/*
 *	sorting
 */
void mmax (header *hd);
void mmin (header *hd);
void msort (header *hd);
void mmax1 (header *hd);
void mmin1 (header *hd);

/*
 *	programming
 */
void margn (header *hd);
void margs (header *hd);
void margs0 (header *hd);


void minterpret (header *hd);
void mevaluate (header *hd);

/*
 *	events
 */
void mwait (header *hd);
void mkey (header *hd);
void mcode (header *hd);

void minput (header *hd);
void mlineinput (header *hd);

/*
 *	utilities : files, dir
 */
void mcd (header *hd);
void mdir (header *hd);
void mdir0 (header *hd);
void msetkey (header *hd);
void mchar (header *hd);
void mascii (header *hd);
void merror (header *hd);
void merrlevel (header *hd);
void mprintf (header *hd);
void mname (header *hd);

#ifndef NOSHRINK
void mshrink (header *hd);
#endif

#ifdef WAVES
void mplaywav (header *hd);
#endif

#endif
