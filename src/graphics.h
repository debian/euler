/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : graphics.h -- portable advanced graphics
 */

#ifndef _GRAPHICS_H_
#define _GRAPHICS_H_

#include "stack.h"

extern int fillcolor1,fillcolor2;
extern int markerfactor;

void ghold (void);
void show_graphics (void);

void mwindow (header *hd);
void mwindow0 (header *hd);
void mclip (header *hd);
void mclip0 (header *hd);
void mholding (header *hd);
void mholding0 (header *hd);
void mscale (header *hd);
void mscaling (header *hd);
void mkeepsquare (header *hd);
void mpixel (header *hd);
void mantialiasing (header *hd);
void mantialiasing0 (header *hd);
/*
 *	2d plots
 */
void mplotarea (header *hd);
void mplot (header *hd);
void mplot1 (header *hd);
void msetplot (header *hd);
void mstyle (header *hd);
void mcolor (header *hd);
void mlstyle (header *hd);
void mlinew (header *hd);

void mframe (header *hd);
void mfcolor (header *hd);

/*
 *	2d markers
 */
void mmark (header *hd);
void mmarkersize (header *hd);
void mmstyle (header *hd);

/*
 *	bars
 */
void mbar (header *hd);
void mbarcolor (header *hd);
void mbarstyle (header *hd);

/*
 *	3d plots
 */
void mcontour (header *hd);
void mdensity (header *hd);
void mdcolor (header *hd);
void mdgrid (header *hd);

void mwire (header *hd);
void mwcolor (header *hd);

void msolid (header *hd);
void msolid1 (header *hd);
void msolidh (header *hd);
void mmesh (header *);
void mmeshflat (header *hd);
void mmeshfactor (header *hd);
void mfillcolor (header *hd);
void mtwosides (header *hd);

void mview (header *hd);
void mview0 (header *hd);
void mproject (header *hd);

/*
 *	text
 */
void mtext (header *hd);
void mctext (header *hd);
void mrtext (header *hd);

void mvtext (header *hd);
void mvctext (header *hd);
void mvrtext (header *hd);

void mvutext (header *hd);
void mvcutext (header *hd);
void mvrutext (header *hd);

void mtextsize (header *hd);
void mtcolor (header *hd);

/*
 *	events
 */
void mmouse (header *hd);

/*
 *	postscript
 */
void mpswindow (header *hd);

#endif
