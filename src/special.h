/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : special.h -- special math functions
 */

#ifndef _SPECIAL_H_
#define _SPECIAL_H_

#include "stack.h"

void mgammaln (header *hd);
void mgamma (header *hd);
void mgammai (header *hd);

void mgauss (header *hd);
void mrandom (header *hd);
void mshuffle (header *hd);
void mnormalnew (header *hd);
void mseed (header *hd);

void mbetai (header *hd);
void mbesselj (header *hd);
void mbessely (header *hd);
void mbesselall (header *hdx);
void mbesseli (header *hd);
void mbesselk (header *hd);
void mbesselmodall (header *hdx);

#endif
