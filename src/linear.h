/*
 *	Euler - a numerical lab
 *
 *	platform : all
 *
 *	file : linear.h -- linear algebra matrix functions
 */

#ifndef _LINEAR_H_
#define _LINEAR_H_

#include "stack.h"

typedef double complex[2];

void c_add (complex x, complex y, complex z);
void c_sub (complex x, complex y, complex z);
void c_div (complex x, complex y, complex z);
void c_mult (complex x, complex y, complex z);
void c_copy (complex x, complex y);

void msimplex (header *hd);

void msolve (header *hd);
void mlu (header *hd);
void mlusolve (header *hd);

void mtridiag (header *hd);
void mjacobi (header *hd);
void msvd (header *hd);

void msolvetoeplitz (header *hd);
void mtoeplitz (header *hd);

void mcharpoly (header *hd);

#endif
