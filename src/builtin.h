/*
 *	Euler - a numerical lab
 *
 *	platform : neutral
 *
 *	file : builtin.h -- euler function table
 */

#ifndef _BUILTIN_H_
#define _BUILTIN_H_

#include "stack.h"

typedef struct {
	char *name;
	int nargs;
	void (*f) (header *);
} builtintyp;

extern int builtin_count;
extern builtintyp builtin_list[];

void sort_builtin (void);
int exec_builtin (char *name, int nargs, header *hd);
builtintyp *find_builtin (char *name);

#endif
